<?php
/**
 * @file
 * Contains the hook definitions necessary to enable drush function munin-run.
 */

/**
 * Implements hook_drush_command().
 */
function munin_api_drush_command() {
  $items = array();

  $items['munin-run'] = array(
    'callback' => 'munin_api_run_drush_callback',
    'arguments' => array(
      'plugin' => 'Run munin plugin.',
    ),
    'options' => array(
      'get-config' => 'Get munin plugin configuration options.',
    ),
    'description' => 'Run munin plugins.',
    'examples' => array(
      'drush munin-run plugin_name' => 'Run munin plugin.',
    ),
    'drupal dependencies' => array('munin_api'),
  );

  $items['munin-setup'] = array(
    'callback' => 'munin_api_setup_drush_callback',
    'description' => 'Produces script output which can be saved and the run as root.',
    'examples' => array(
      'drush munin-setup' => 'Produces a setup script.',
      'drush munin-setup | sudo sh' => 'Run the setup script as root.',
    ),
    'drupal dependencies' => array('munin_api'),
  );

  $items['munin-cleanup'] = array(
    'callback' => 'munin_api_cleanup_drush_callback',
    'description' => 'Cleans up scripts and symlinks.',
    'examples' => array(
      'drush munin-cleanup' => 'Produces a cleanup script.',
      'drush munin-cleanup | sudo sh' => 'Run the cleanup script as root.',
    ),
    'drupal dependencies' => array('munin_api'),
  );

  return $items;
}

/**
 * Drush callback function which will output the commands to setup munin.
 */
function munin_api_setup_drush_callback() {

//  cat <<EOF > {$script_file}
//{$script_contents}
//EOF

  // Targets
  $site = check_plain(munin_api_site());
  $script_dir = '/usr/local/munin/plugins';
  $script_file = "$script_dir/$site";
  $plugins_dir = '/etc/munin/plugins';
  $conf_dir = '/etc/munin/plugin-conf.d';
  $script_contents = munin_api_script();
  $the_plugins = "{$plugins_dir}/{$site}_*";
  $drush_user = munin_api_drush_user();
  $restart = '/etc/init.d/munin-node restart';
  // Symlinks
  $syms = '';
  foreach (munin_api_plugin_definitions(TRUE) as $plugin_name => $plugin_parts) {
    $syms .= "echo 'Create symlink: {$site}_{$plugin_name}'\nln -s {$script_file} {$plugins_dir}/{$site}_{$plugin_name}\n";
  }
  // Build it
  return <<<COMMANDS
#! /bin/bash
# Munin setup script
# Review the contents of this file and run as root.
echo 'Making script directory: {$script_dir}'
mkdir -p {$script_dir}
echo 'Writing the script: {$script_file}'
echo '{$script_contents}' > {$script_file}
echo 'Configuring to run as correct user: {$drush_user}'
cat <<EOF > $conf_dir/{$site}
[{$site}_*]
user {$drush_user}
EOF
echo 'Making executable: {$script_file}'
chmod a+x {$script_file}
echo 'Deleting symlinks: {$the_plugins}'
rm -f {$the_plugins}
{$syms}
echo 'Restarting munin-node.'
$restart
echo 'Munin setup completed. Happy monitoring.'

COMMANDS;
}

/**
 * Drush callback function which will output the commands to setup munin.
 */
function munin_api_cleanup_drush_callback() {
  // Targets
  $site = check_plain(munin_api_site());
  $script_dir = '/usr/local/munin/plugins';
  $script_file = "$script_dir/$site";
  $plugins_dir = '/etc/munin/plugins';
  $conf_dir = '/etc/munin/plugin-conf.d';
  $the_plugins = "{$plugins_dir}/{$site}_*";
  $restart = '/etc/init.d/munin-node restart';
  // Build it
  return <<<COMMANDS
#! /bin/bash
# Munin cleanup script
# Review the contents of this file and run as root.
echo 'Deleting symlinks: {$the_plugins}'
rm -f {$the_plugins}
echo 'Deleting script: {$script_file}'
rm -f {$script_file}
echo 'Deleting conf: $conf_dir/{$site}'
rm -f $conf_dir/{$site}
echo 'Restarting munin-node.'
$restart
echo 'Munin cleanup completed. You can restore anytime by running drush munin-setup again. Thanks for monitoring.'

COMMANDS;
}

/**
 * Callback function that creats the response for munin to read and create the
 * graphs. This function is executed when drush gets munin-run argument (See
 * munin_api_drush_command).
 *
 * @param type $plugin_name
 *   The name of the called plugin. Can contain or not drupal_ prefix (it will
 * be detected and removed).
 */
function munin_api_run_drush_callback($plugin_name = NULL) {
  $definitions = munin_api_plugin_definitions(TRUE);
  $plugins = array();

  if ($plugin_name) {
    $plugin_name = array_pop(explode('_', $plugin_name));
    if ($plugin_name) {
      $plugins = array($plugin_name);
    }
  }
  elseif (!count($plugins)) {
    $plugins = array_keys($definitions);
  }

  foreach ($plugins as $plugin_name) {
    if (!$definitions[$plugin_name]) {
      drush_print(dt('Plugin not available or not enabled.'));
    }
    else {
      // We are requested to print cofiguration settings for this plugin.
      if (drush_get_option('get-config', FALSE)) {
        drush_print(munin_api_get_config($definitions[$plugin_name], $plugin_name));
      }
      // We are requested to print the value for this plugin.
      else {
        drush_print(munin_api_get_values($definitions[$plugin_name], $plugin_name));
      }
    }
  }
}
